from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name="home"),
    path('manual/', views.manual, name = "manual"),
    path('manual/processing/', views.processing, name="processing"),
    path('bubblesort/', views.bubblesort, name="bubblesort"),
]
