from django import forms

ALGORITHM_CHOICES = [
    ('bubbleSort', 'bubbleSort'),
    ('insertionSort', 'insertionSort'),
    ('selectionSort', 'selectionSort'),
    ('mergeSort', 'mergeSort'),
    ('quickSort', 'quickSort'),
]


class sortingForm(forms.Form):
    numbers = forms.CharField(label='numbers', max_length = 100)
    algorithm = forms.CharField(label='Select your algorithm', widget=forms.Select(choices=ALGORITHM_CHOICES))
