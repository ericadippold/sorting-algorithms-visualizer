
import random
from django.http import HttpResponse
from django.shortcuts import render


from algorithms.forms import sortingForm

def quickSort(nts):
    nts_len = len(nts)
    #Base Case
    if nts_len < 2:
        return nts
    current_position = 0 #position of the partitioning element
    for i in range(1, nts_len): # partitioning loop
        if nts[i] <= nts[0]:
            current_position += 1
            temp = nts[i]
            nts[i] = nts[current_position]
            nts[current_position] = temp
    temp = nts[0]
    nts[0] = nts[current_position]
    nts[current_position] = temp # brings pivot to it's appropriate position

    left = quickSort(nts[0:current_position]) # sorts elements to the left of pivot
    right = quickSort(nts[current_position+1:nts_len]) # sorts elements to right of the pivot

    nts = left + [nts[current_position]] + right # merging everything together

    return nts

def mergesort(request):
    animations = getMergeSortAnimation(arr)
    return HttpResponse([animations])

def getMergeSortAnimation(arr):
    animations = []


def mergeSort(nts):
    # store the length of the list
    nts_len = len(nts)
    # list with length less than is already sorted
    if nts_len == 1:
        return nts
    # identify the list midpoint and partition the list into a left_partition and a right_partition
    mid_point = nts_len // 2
    # to ensure all partitions are broken down into their individual components,
        # the marge_sort function is called and a partitioned portion of the list is passed as a parameter
    left_partition = mergeSort(nts[:mid_point])
    right_partition = mergeSort(nts[mid_point:])
    # the merge_sort function returns a list composed of a sorted left and right partition
    return merge(left_partition, right_partition)

    # takes in two lists and returns a sorted list made up of the content within the two lists
def merge(left, right):
    # initialize an empty list output that will be populated with sorted elements
    # initalize two variables i and j which are used as pointers when iterating through the lists
    output = []
    i = j = 0
    # executes the while loop if both pointers i and j are less than the length of the left and right lists
    while i < len(left) and j < len(right):
        # compare the elements at every position of both lists during each iteration
        if left[i] < right[j]:
            # output is populated with the lesser value
            output.append(left[i])
            # move pointer to the right
            i += 1
        else:
            output.append(right[j])
            j += 1
    # the remnant elements are picked from the current pointer value to the end of the respective list
    output.extend(left[i:])
    output.extend(right[j:])

    return output


def selectionSort(nts):
    n = len(nts)
    for i in range(n):
        idx = i
        for p in range(i + 1, n):
            if nts[p] < nts[idx]:
                idx = p
        nts[i], nts[idx] = nts[idx], nts[i]
    return nts


def insertionSort(nts):
    # loop through range starting at position 1.
    for i in range(1, len(nts)):
        # we are going to get the number in position 1
        current_num = nts[i]
        # now i need to get position 0 so I can compare 1 to 0.
        p = i - 1
        # while p >= 0 and the item at position 0 greater than the current number at i (position 1)
        while p >= 0 and nts[p] > current_num:
            # swap the item in position 1 with the item in position 0
            nts[p + 1] = nts[p]
            # decrement p by 1 (so now it stores -1 on the first iteration and breaks)
            p -= 1
        # now since p has been changed to -1, we have to add one to it and make it equal to the current_num
        nts[p + 1] = current_num
    return nts

def arrayValues():
    ary = []
    for i in range(0, 120):
        n = random.randint(100, 400)
        ary.append(n)
    return ary

def index(request):
    global arr
    arr = arrayValues()
    return render(request, "algorithms/home.html", {'arr': arr})

def bubblesort(request):
    animations = getBubbleSortAnimations(arr)
    return HttpResponse([animations])

def getBubbleSortAnimations(arr):
    animations = []
    auxiliaryArray = arr.copy()
    bubbleSortHelper(auxiliaryArray, animations)
    return animations

def bubbleSortHelper(auxiliaryArray, animations):
    for i in range(0, len(auxiliaryArray) - 1):
        for j in range(0, len(auxiliaryArray) - 1):
            animations.append([j, j+1])
            animations.append([j, j+1])
            if auxiliaryArray[j] > auxiliaryArray[j+1]:
                animations.append([j, auxiliaryArray[j+1]])
                animations.append([j+1, auxiliaryArray[j]])
                auxiliaryArray[j], auxiliaryArray[j+1] = auxiliaryArray[j+1], auxiliaryArray[j]
            else:
                animations.append([-1, -1])
                animations.append([-1, -1])


def bubbleSort(nts):
    # find the length of our numbers (nts = numbers to sort) for # of looping of list
    nts_len = len(nts)
    # loop through over our list the # of times based on the length
    for i in range(nts_len):
        # now in inner loop, range is length - i (i.e. 10 - 0 - 1 == a range of 0, 9) subtracting i and then the last elem as well. p in the second iteration is going to be in position 1 instead of 0
        for p in range(nts_len - i - 1):
            # now check first number in position 0. (first loop) and now compare if that is greater than position 1 on my list.
            if nts[p] > nts[p + 1]:
                # if this is true, we want to replace them
                nts[p], nts[p + 1] = nts[p + 1], nts[p]
    return nts

def manual(request):
    form = sortingForm()
    return render(request, "algorithms/manual.html", {'form': form })

def processing(request):
    # if the method was post (aka - coming from our form)
    if request.method == 'POST':
        #creating a form instance and populate it with the data from our request
        form = sortingForm(request.POST)
        # if form is valid
        if form.is_valid():
            # grabbing numbers from sortingForm attribute
            x = request.POST['numbers']
            # get algorithm data from sortingForm attribute class
            y = request.POST['algorithm']
            # split puts it in the format our sorting algo needs, list form 1 2 3 --> [1, 2, 3]
            z = x.split()
            # loop through and make sure they are integers
            map_object = map(int, z)
            # format map object into list
            list_of_integers = list(map_object)
            # now we have to run the data through our algorithm
            # connect to y to select the algorithm by name. And then start the algorithm by passing the data into it w/ list_of_integers
            data = globals()[y](list_of_integers)

    #has to store data that has been captured from the form
    return render(request, "algorithms/processing.html", {'data':data})

########
